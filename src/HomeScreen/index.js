import React, { Component } from "react";
import HomeScreen from "./HomeScreen.js";
import CartScreen from "../Cart/index.js";
import OrderScreen from "../Order/index.js";
import PaymentScreen from "../Payment/index.js";
import OrderHistoryScreen from "../OrderHistory/index.js";
import RegisterScreen from "../Register/index.js";

import ReOrderScreen from "../Reorder/index.js";
import ReturnScreen from "../Return/index.js";
import ReturnOrderScreen from "../ReturnOrder/index.js";
import SideBar from "../SideBar/SideBar.js";
import { DrawerNavigator } from "react-navigation";

const HomeScreenRouter = DrawerNavigator(
  {
    홈: { screen: HomeScreen, title:" 홈" },
    Cart: { screen: CartScreen },
    Order: { screen: OrderScreen },
    Payment: { screen: PaymentScreen },
    OrderHistory: { screen: OrderHistoryScreen },
    Register: { screen: RegisterScreen },
    Reorder: { screen: ReOrderScreen },
    Return: { screen: ReturnScreen },
    ReturnOrder: { screen: ReturnOrderScreen },
  },
  {
    contentComponent: props => <SideBar {...props} />
  }
);
export default HomeScreenRouter;

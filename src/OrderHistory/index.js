import React, { Component } from "react";
import OrderHistory from "./OrderHistoryScreen";
import { StackNavigator } from "react-navigation";
export default (DrawNav = StackNavigator(
  {
    주문내역:OrderHistory
  },
  {
    initialRouteName: "주문내역"
  }
));

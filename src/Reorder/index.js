import React, { Component } from "react";
import Reorder from "./ReorderScreen";
import { StackNavigator } from "react-navigation";
export default (DrawNav = StackNavigator(
  {
    Reorder:Reorder
  },
  {
    initialRouteName: "Reorder"
  }
));
